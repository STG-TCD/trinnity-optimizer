{-
Copyright 2017 the Provost, Fellows, Foundation Scholars and the other members
of Board, of the College of the Holy and Undivided Trinity of Queen Elizabeth
near Dublin
-}

{-# LANGUAGE ScopedTypeVariables, DoAndIfThenElse #-}
module Main where

import Prelude hiding (elem)

import System.IO (hPutStrLn, stderr)
import System.Environment (getArgs)
import Control.Monad (when, liftM, zipWithM_)
import Data.List.Split (splitOn)
import Data.Char (isSpace, toUpper)
import Data.List (transpose, find, minimumBy, intercalate, zip5, zip4, zip3, zipWith3, isInfixOf, elemIndex, elem, all, intersect)
import Data.List.Extras (argmin)
import Data.Maybe (isJust, fromJust, isNothing)
import Data.Ord (comparing)
import Options.Applicative (auto, execParser, footer, fullDesc, helper, help, short, long, metavar, option, info, progDesc, hsubparser, strOption, value, command, ParserInfo, Parser)
import qualified Data.Map as Map

import qualified Data.Map as Map

data Command = HeuristicCommand HeuristicOptions
             | PBQPCommand PBQPOptions
             | InstantiateCommand InstantiateOptions
             | ListHeuristicsCommand
             deriving Show

data Options = Options { ocommand :: Command } deriving Show

data Heuristic = SUM2D_CHW
               | SUM2D_HWC
               | DIRECT
               | PATCH_GEMM
               | GEMM
               | WINOGRAD
               | FFT
               | LOCAL_OPTIMAL
               | LOCAL_OPTIMAL_CHW
               | LOCAL_OPTIMAL_HWC
               | LOCAL_OPTIMAL_HCW
               deriving (Read, Show, Eq)

data HeuristicOptions = HOptions {
                                   heuristic :: Heuristic
                                 , htopology :: String
                                 , hlayouts :: String
                                 , nodecosts :: String
                                 , hcostscale :: Double
                                 , hcutoff :: Double
                                 }
                                 deriving Show

data PBQPOptions = POptions {
                              ptopology :: String
                            , selectionConstraints :: String
                            , layoutConstraints :: String
                            , pnodes :: String
                            , pedges :: String
                            , costscale :: Double
                            , pcutoff :: Double
                            }
                            deriving Show

data Implementation = MKLDNN
                    | ARMCL
                    | TRINNITY
                    deriving (Read, Show, Eq)

data InstantiateOptions = IOptions {
                                     itopology :: String
                                   , ilayouts :: String
                                   , imethods :: String
                                   , solution :: String
                                   , iextlib :: Implementation
                                   , layerIDs :: String
                                   }
                                   deriving Show

hcommand :: Parser Command
hcommand = HeuristicCommand <$> heuristicOptionParser

heuristicOptionParser :: Parser HeuristicOptions
heuristicOptionParser = HOptions
  <$> option auto
      ( long "heuristic"
      <> short 'x'
      <> metavar "HEURISTIC"
      <> value LOCAL_OPTIMAL
      <> help "Which heuristic to use for selection" )
  <*> strOption
      ( long "topology"
      <> short 't'
      <> metavar "TOPOLOGY"
      <> help "Read network topology description from this file" )
  <*> strOption
      ( long "algorithms"
      <> short 'a'
      <> metavar "ALGORITHMS"
      <> help "Read algorithm definitions and input/output layout descriptors from this file" )
  <*> strOption
      ( long "nodes"
      <> short 'n'
      <> metavar "NODE"
      <> help "Read node (layer) costs from this file" )
  <*> option auto
      ( long "cost-scale"
      <> short 'c'
      <> metavar "COST_SCALE"
      <> value 1.0
      <> help "Premultiplier for costs in the input CSV" )
  <*> option auto
      ( long "cutoff"
      <> short 'x'
      <> metavar "CUTOFF"
      <> value 1.0
      <> help "Threshold time below which we should mark an algorithm unselectable" )

pcommand :: Parser Command
pcommand = PBQPCommand <$> pbqpOptionParser

pbqpOptionParser :: Parser PBQPOptions
pbqpOptionParser = POptions
  <$> strOption
      ( long "topology"
      <> short 't'
      <> metavar "TOPOLOGY"
      <> help "Read network topology description from this file" )
  <*> strOption
      ( long "selection-constraints"
      <> short 's'
      <> metavar "SELECTION_CONSTRAINT"
      <> help "Read selection constraints for nodes from this file" )
  <*> strOption
      ( long "algorithms"
      <> short 'a'
      <> metavar "ALGORITHMS"
      <> help "Read algorithm definitions and input/output layout descriptors from this file" )
  <*> strOption
      ( long "nodes"
      <> short 'n'
      <> metavar "NODE"
      <> help "Read node (layer) costs from this file" )
  <*> strOption
      ( long "edges"
      <> short 'e'
      <> metavar "EDGE"
      <> help "Read edge (offload/data transformation) costs from this file" )
  <*> option auto
      ( long "cost-scale"
      <> short 'c'
      <> metavar "COST_SCALE"
      <> value 1.0
      <> help "Premultiplier for costs in the input CSV" )
  <*> option auto
      ( long "cutoff"
      <> short 'x'
      <> metavar "CUTOFF"
      <> value 1.0
      <> help "Threshold time below which we should mark an algorithm unselectable" )

icommand :: Parser Command
icommand = InstantiateCommand <$> instantiateOptionParser

instantiateOptionParser :: Parser InstantiateOptions
instantiateOptionParser = IOptions
  <$> strOption
      ( long "topology"
      <> short 't'
      <> metavar "TOPOLOGY"
      <> help "Read network topology description from this file" )
  <*> strOption
      ( long "algorithms"
      <> short 'a'
      <> metavar "ALGORITHMS"
      <> help "Read algorithm definitions and input/output layout descriptors from this file" )
  <*> strOption
      ( long "methods"
      <> short 'm'
      <> metavar "METHOD"
      <> help "Read methods from this file" )
  <*> strOption
      ( long "solution"
      <> short 's'
      <> metavar "SOLUTION"
      <> help "Read the optimizer solution from this file" )
  <*> option auto
      ( long "implementation"
      <> short 'i'
      <> metavar "IMPLEMENTATION"
      <> value TRINNITY
      <> help "What implementation library to use to instantiate the network" )
  <*> strOption
      ( long "layer-identifiers"
      <> short 'l'
      <> metavar "LAYER_IDS"
      <> help "Read the names of convolution layers from this file" )

lhcommand :: Parser Command
lhcommand = pure ListHeuristicsCommand

optParser :: Parser Options
optParser = Options
  <$> hsubparser
      (  (command "heuristic" (info hcommand (progDesc "Generate a heuristic solution")))
      <> (command "pbqp" (info pcommand (progDesc "Generate a PBPQ query to feed to a solver")))
      <> (command "instantiate" (info icommand (progDesc "Generate an instantion of the network given a solution")))
      <> (command "list-heuristics" (info lhcommand (progDesc "List the available heuristics")))
      )

options :: ParserInfo Options
options = info (helper <*> optParser) (fullDesc <> footer "For help with a specific command, do triNNity-optimizer COMMAND --help")

main = do
  opts <- execParser options
  case (ocommand opts) of
    (HeuristicCommand (HOptions h t l n c x)) -> heuristicMode h t l n c x
    (PBQPCommand (POptions t s l n e c x)) -> pbqpMode t s l n e c x
    (InstantiateCommand (IOptions t l m s i x)) -> instantiateMode i t l m s x
    (ListHeuristicsCommand) -> listHeuristics

listHeuristics = do
  putStrLn "Available Heuristics: "
  putStrLn "  SUM2D_CHW: use the sum2d-chw algorithm for each convolution"
  putStrLn "  SUM2D_HWC: use the sum2d-hwc algorithm for each convolution"
  putStrLn "  DIRECT: use the fastest direct algorithm for each convolution"
  putStrLn "  PATCH_GEMM: use the fastest patch matrix based GEMM algorithm for each convolution"
  putStrLn "  GEMM: use the fastest pure GEMM (no patch matrix) algorithm for each convolution"
  putStrLn "  WINOGRAD: use the fastest Winograd algorithm for each convolution"
  putStrLn "  FFT: use the fastest FFT algorithm for each convolution"
  putStrLn "  LOCAL_OPTIMAL: use the fastest algorithm from any class for each convolution, ignoring data layout transformation costs"
  putStrLn "  LOCAL_OPTIMAL_CHW: use the fastest algorithm operating on the specified layout for each convolution"
  putStrLn "  LOCAL_OPTIMAL_HWC: use the fastest algorithm operating on the specified layout for each convolution"
  putStrLn "  LOCAL_OPTIMAL_HCW: use the fastest algorithm operating on the specified layout for each convolution"
  putStrLn "  RELAX_RUNTIME: start with the fastest network and relax time to meet memory target"
  putStrLn "  RELAX_MEMORY: start with the smallest network and relax memory to meet time target"

scenarioKeys = ["KERNELS","CHANNELS","STRIDE","WIDTH","HEIGHT","K_W","K_H","SPARSITY"]

data Scenario = Scenario {
                           kernels :: Int,
                           channels :: Int,
                           stride :: Int,
                           width :: Int,
                           height :: Int,
                           k_w :: Int,
                           k_h :: Int,
                           sparsity :: Int
                         } deriving (Eq, Ord, Read, Show)

fromList :: [Int] -> Scenario
fromList [kn, cn, st, wi, he, kw, kh, sp] = Scenario kn cn st wi he kw kh sp
fromList x@(_) = error $ "Couldn't build scenario description for input: " ++ show x

norm :: Scenario -> Scenario -> Int
norm a b =
  let aParams = [kernels a, channels a, stride a, width a, height a, k_w a, k_h a, sparsity a]
      bParams = [kernels b, channels b, stride b, width b, height b, k_w b, k_h b, sparsity b]
  in sum $ map abs $ zipWith subtract (map abs aParams) (map abs bParams)

maxCost :: Double
maxCost = (2 ** 32) - 1 -- maximum cost for PBQP

minCost :: Double
minCost = -(2 ** 32) -- minimum cost for PBQP

isConvolution :: Scenario -> Bool
isConvolution s = (odd (k_w s)) && (odd (k_h s))

available :: String -> Scenario -> Bool
available algoName scenario
  | (isInfixOf "conv-1x1" algoName) = ((k_w scenario) == 1) && ((k_h scenario) == 1) && ((stride scenario) == 1)
  | (isInfixOf "scan" algoName) = (stride scenario) == 1
  | (isInfixOf "im2col-copy-short" algoName) = (stride scenario) == 1
  | (isInfixOf "im2col-copy-long" algoName) = (stride scenario) == 1
  | (isInfixOf "wino" algoName) =
      ((k_w scenario) `elem` [3, 5]) &&
      ((k_h scenario) `elem` [3, 5])
  | otherwise = True

adjustCost :: Double -> Double -> Double -> Double -> Scenario -> String -> Double -> Double
adjustCost maxCost minCost scale threshold scenario algo initCost =
  if isConvolution scenario then
      adjustC algo scenario initCost
  else 0
  where
    adjustC a scenario c =
      if available a scenario then
        if (c*scale) < threshold then
          maxCost
        else c*scale
      else maxCost

parseCSV :: String -> [[String]]
parseCSV f = map (map (takeWhile (not . isSpace) . dropWhile isSpace) . splitOn ",") $ filter (not . null) $ lines f

parseMethodCSV :: String -> [(String, String)]
parseMethodCSV f =
  let pairs = map (take 2 . map (takeWhile (not . isSpace) . dropWhile isSpace) . splitOn ",") $ filter (not . null) $ lines f
  in map (\[a, b] -> (a, b)) pairs

heuristicMode heuristic topoPath algorithmsCSVPath nodeCostCSVPath costScale unselectableThreshold = do
  topoFile <- liftM (filter (not . null) . lines) $ readFile topoPath
  let [nodes, parameters, edges] :: [Int] = take 3 $ map read $ words $ topoFile !! 0
  let topoNodes :: [Scenario] = map read $ take nodes $ drop 1 topoFile
  let topoEdges :: [(Int, Int)] = map (\[a, b] -> (read a, read b)) $ map words $ take edges $ drop (1+nodes) topoFile

  algorithmsCSVFile <- readFile algorithmsCSVPath
  let algorithmsCSVText :: [[String]] = parseCSV algorithmsCSVFile
  let algorithmsCSVData :: [[String]] = algorithmsCSVText
  let knownAlgorithms :: [String] = map head algorithmsCSVData

  nodeCostCSVFile <- readFile nodeCostCSVPath
  let parsednodeCostCSVFile :: [[String]] = parseCSV nodeCostCSVFile
  let (headerText, nodeCostCSVText) :: ([[String]], [[String]]) = splitAt (length scenarioKeys) parsednodeCostCSVFile
  let adjustednodeCostCSVText :: [[String]] = transpose $ map tail $ filter ((`elem` knownAlgorithms) . head) nodeCostCSVText -- discard data rows with unrecognized algorithms
  let adjustedHeaderCSVText :: [[String]] = transpose $ map tail $ filter ((`elem` scenarioKeys) . head) headerText -- discard header rows with unrecognized labels
  let nodeCostCSVData :: [(Scenario, [Double])] = zipWith (\header costs -> ((fromList $ map read header), map read costs)) adjustedHeaderCSVText adjustednodeCostCSVText

  if any (all (==0.0) . snd) nodeCostCSVData then do
    let brokenScenarios = filter (all (==0.0) . snd) nodeCostCSVData
    hPutStrLn stderr "Error: no valid algorithm for some scenarios in cost model"
    mapM_ (hPutStrLn stderr . show) $ brokenScenarios

  else do
    let nodeCostCSVLabels :: [String] = map head $ filter ((`elem` knownAlgorithms) . head) nodeCostCSVText
    let adjustednodeCostCSVData :: Map.Map Scenario [Double] = Map.fromList $ map (\(x, y) -> (x, zipWith (adjustCost maxCost minCost costScale unselectableThreshold x) nodeCostCSVLabels y)) nodeCostCSVData

    -- hPutStrLn stderr "Adjusted cost tables for selection:"
    -- mapM_ (hPutStrLn stderr . show) $ Map.assocs adjustednodeCostCSVData

    let benchmarkedAlgorithmNames = map head algorithmsCSVData

    let candidateLabels = case heuristic of {
      SUM2D_CHW -> [];
      SUM2D_HWC -> [];
      DIRECT -> filter (isInfixOf "direct") benchmarkedAlgorithmNames;
      PATCH_GEMM -> (filter (isInfixOf "im2") benchmarkedAlgorithmNames) ++ (filter (isInfixOf "mec") benchmarkedAlgorithmNames) ++ filter (isInfixOf "sum2d") benchmarkedAlgorithmNames;
      GEMM  -> filter (isInfixOf "kn2") benchmarkedAlgorithmNames ++ filter (isInfixOf "sum2d") benchmarkedAlgorithmNames;
      WINOGRAD  -> filter (isInfixOf "wino") benchmarkedAlgorithmNames ++ filter (isInfixOf "sum2d") benchmarkedAlgorithmNames;
      FFT  -> filter (isInfixOf "fft") benchmarkedAlgorithmNames ++ filter (isInfixOf "sum2d") benchmarkedAlgorithmNames;
      LOCAL_OPTIMAL -> benchmarkedAlgorithmNames;
      LOCAL_OPTIMAL_CHW -> benchmarkedAlgorithmNames;
      LOCAL_OPTIMAL_HWC -> benchmarkedAlgorithmNames;
      LOCAL_OPTIMAL_HCW -> benchmarkedAlgorithmNames;
    }

    if null candidateLabels then do
      hPutStrLn stderr "warning: selecting sum2d for all nodes"
      let sum2DIndex = case heuristic of { SUM2D_CHW -> fromJust $ elemIndex "direct-sum2d-chw" benchmarkedAlgorithmNames;
                                           SUM2D_HWC -> fromJust $ elemIndex "direct-sum2d-hwc" benchmarkedAlgorithmNames; }
      let solution = map (const sum2DIndex) topoNodes
      let predictedCost = 0.0
      mapM_ (putStrLn . show) solution
      putStrLn $ show $ round predictedCost
      putStrLn $ show 0
    else do
      let (solution, pcosts) = unzip $ map (selectHeuristic candidateLabels benchmarkedAlgorithmNames adjustednodeCostCSVData) $ zip [0..] topoNodes
      -- TODO: add in the edge costs as well --- need to look up the transform implied by each node pair in the graph
      let predictedCost = sum pcosts
      mapM_ (putStrLn . show) solution
      putStrLn $ show $ round predictedCost
      putStrLn $ show 0
    where
      fromJustOrError e m =
        case m of
          (Just x) -> x
          _ -> error e

      -- returns the index and projected cost of the selected algorithm
      selectHeuristic :: [String] -> [String] -> Map.Map Scenario [Double] -> (Int, Scenario) -> (Int, Double)
      selectHeuristic candidates allAlgorithms costs (scenarioIndex, scenarioParams) =
        if isConvolution scenarioParams then
          if (isNothing $ Map.lookup scenarioParams costs) then
            error $ "Costs missing for topology node " ++ show scenarioParams ++ "\n" ++ "Cost table keys:\n" ++ (intercalate "\n" $ fmap show $ Map.keys costs)
          else
            let allAlgorithmsWithCosts = zip allAlgorithms $ fromJust $ Map.lookup scenarioParams costs
                availableAlgorithms = filter ((`elem` candidates) . fst) allAlgorithmsWithCosts
                fastestAvailable = minimumBy (comparing snd) availableAlgorithms
                algoIndex = fromJust $ elemIndex (fst fastestAvailable) allAlgorithms
                algoCost = snd fastestAvailable
            in (algoIndex, algoCost)
        else
            (0, minCost) -- non-convolutional nodes don't have entries in the cost table

data PBQPFormat = PBQPFormat { nodesCount :: Int
                             , edgesCount :: Int
                             , nodes :: [PBQPNode]
                             , edges :: [PBQPEdge]
                             }

instance Show PBQPFormat where
  show (PBQPFormat n e ns es) = show n ++ " " ++
                                show e ++ "\n" ++
                                (intercalate "\n" $ map show ns) ++ "\n" ++
                                (intercalate "\n" $ map show es)

data PBQPNode = PBQPNode { nodeWeightsCount :: Int
                         , nodeWeights :: [Double]
                         }

instance Show PBQPNode where
  show (PBQPNode nwc nw) = show nwc ++ "\n" ++
                           (intercalate " " $ map (show . round) nw)

data PBQPEdge = PBQPEdge { from :: Int
                         , to :: Int
                         , fromWeightsCount :: Int
                         , toWeightsCount :: Int
                         , fromToWeights :: [Double]
                         }

instance Show PBQPEdge where
  show (PBQPEdge f t fwc twc ftw) = show f ++ " " ++
                                    show t ++ "\n" ++
                                    show fwc ++ " " ++
                                    show twc ++ "\n" ++
                                    (intercalate " " $ map (show . round) ftw)

pbqpMode topoPath selectionConstraintsPath algorithmsCSVPath nodeCostCSVPath edgeCostCSVPath costScale unselectableThreshold = do
  topoFile <- liftM (filter (not . null) . lines) $ readFile topoPath
  let [nodes, parameters, edges] :: [Int] = take 3 $ map read $ words $ topoFile !! 0
  let topoNodes :: [Scenario] = map read $ take nodes $ drop 1 topoFile
  let topoEdges :: [(Int, Int)] = map (\[a, b] -> (read a, read b)) $ map words $ take edges $ drop (1+nodes) topoFile

  selectionConstraintsFile <- readFile selectionConstraintsPath
  let selectionConstraintsText :: [[String]] = parseCSV selectionConstraintsFile
  let selectionConstraintsData :: [(Int, [String])] = map (\x -> (read $ head x, tail x)) selectionConstraintsText

  algorithmsCSVFile <- readFile algorithmsCSVPath
  let algorithmsCSVText :: [[String]] = parseCSV algorithmsCSVFile
  let algorithmsCSVData :: [[String]] = algorithmsCSVText
  let knownAlgorithms :: [String] = map head algorithmsCSVData

  nodeCostCSVFile <- readFile nodeCostCSVPath
  let parsednodeCostCSVFile :: [[String]] = parseCSV nodeCostCSVFile
  let (headerText, nodeCostCSVText) :: ([[String]], [[String]]) = splitAt (length scenarioKeys) parsednodeCostCSVFile
  let adjustednodeCostCSVText :: [[String]] = transpose $ map tail $ filter ((`elem` knownAlgorithms) . head) nodeCostCSVText -- discard data rows with unrecognized algorithms
  let adjustedHeaderCSVText :: [[String]] = transpose $ map tail $ filter ((`elem` scenarioKeys) . head) headerText -- discard header rows with unrecognized labels
  let nodeCostCSVData :: [(Scenario, [Double])] = zipWith (\header costs -> ((fromList $ map read header), map read costs)) adjustedHeaderCSVText adjustednodeCostCSVText

  if any (all (==0.0) . snd) nodeCostCSVData then do
    let brokenScenarios = filter (all (==0.0) . snd) nodeCostCSVData
    hPutStrLn stderr "Error: no valid algorithm for some scenarios in cost model"
    mapM_ (hPutStrLn stderr . show) $ brokenScenarios

  else do
    let nodeCostCSVLabels :: [String] = map head $ filter ((`elem` knownAlgorithms) . head) nodeCostCSVText
    let adjustednodeCostCSVData :: Map.Map Scenario [Double] = Map.fromList $ map (\(x, y) -> (x, zipWith (adjustCost maxCost minCost costScale unselectableThreshold x) nodeCostCSVLabels y)) nodeCostCSVData

    -- hPutStrLn stderr "Adjusted cost tables for selection:"
    -- mapM_ (hPutStrLn stderr . show) $ Map.assocs adjustednodeCostCSVData

    let benchmarkedAlgorithms = filter ((`elem` nodeCostCSVLabels) . head) algorithmsCSVData

    edgeCostCSVFile <- readFile edgeCostCSVPath
    let edgeCostCSVText :: [[String]] = parseCSV edgeCostCSVFile
    let edgeCostCSVData :: [((String, [Int]), Double)] = parseEdges costScale edgeCostCSVText

    let pbqpNodes = zipWith (buildPBQPNode nodeCostCSVLabels adjustednodeCostCSVData selectionConstraintsData benchmarkedAlgorithms) [0..] topoNodes

    zipWithM_ warnSynthesizedCosts topoNodes pbqpNodes

    let pbqpNodes' = map (either id id) pbqpNodes
    let pbqpEdges = map (buildPBQPEdge topoNodes edgeCostCSVData benchmarkedAlgorithms) topoEdges

    let pbqpData = PBQPFormat nodes edges pbqpNodes' pbqpEdges
    putStrLn $ show pbqpData
    where
      buildPBQPNode :: [String] -> Map.Map Scenario [Double] -> [(Int, [String])] -> [[String]] -> Int -> Scenario -> Either PBQPNode PBQPNode
      buildPBQPNode algoNames costs constraints benchmarkedAlgorithms scenarioIndex scenarioParams =
        let constraintsForScenario = snd $ constraints !! scenarioIndex
            inPredicate = if (constraintsForScenario !! 0) == "*" then const True else (==(constraintsForScenario !! 0))
            methodPredicate = if (constraintsForScenario !! 1) == "*" then const True else (==(constraintsForScenario !! 1))
            outPredicate = if (constraintsForScenario !! 2) == "*" then const True else (==(constraintsForScenario !! 2))
        in
          if (scenarioIndex >= (length costs)) then -- we have a scenarioIndex that is out of range
            if (isNothing $ Map.lookup scenarioParams costs) then -- we have no cost table entry for this node
              if (isConvolution scenarioParams) then -- it is a convolution
                let adjustedCostsForKW = filter (((k_w scenarioParams)==) . (k_w) . fst) $ Map.toList costs -- only consider scenarios with the same K_W
                    adjustedCostsForKH = filter (((k_h scenarioParams)==) . (k_h) . fst) $ Map.toList costs -- only consider scenarios with the same K_H
                    adjustedCostsForStride = filter (((stride scenarioParams)==) . (stride) . fst) $ Map.toList costs -- only consider scenarios with the same stride
                    adjustedCosts = adjustedCostsForKW `intersect` adjustedCostsForKH `intersect` adjustedCostsForStride -- K_W, K_H, and stride
                    adjustedCosts' = if null adjustedCosts then Map.toList costs else adjustedCosts -- unless there are none!
                    (_, costsForScenario) = argmin (norm scenarioParams . fst) adjustedCosts'
                    algoCosts = zip algoNames costsForScenario
                    methodsWithCosts = zip benchmarkedAlgorithms algoCosts
                    constrainedForMethod = map (\(methodInOut, cost) -> (methodInOut, if methodPredicate $ methodInOut !! 0 then cost else (fst cost, maxCost))) methodsWithCosts
                    constrainedForInput  = map (\(methodInOut, cost) -> (methodInOut, if inPredicate $ methodInOut !! 1 then cost else (fst cost, maxCost))) constrainedForMethod
                    constrainedForOutput = map (\(methodInOut, cost) -> (methodInOut, if outPredicate $ methodInOut !! 2 then cost else (fst cost, maxCost))) constrainedForInput
                    constrainedCosts = map (snd . snd) constrainedForOutput
                in Left $ PBQPNode (length constrainedCosts) constrainedCosts
              else -- this is not a convolution, so emit the default cost for non-convolutional nodes
                Left $ PBQPNode (length benchmarkedAlgorithms) (replicate (length benchmarkedAlgorithms) 0)
            else -- we found a cost table entry for this node
              let costsForScenario = fromJust $ Map.lookup scenarioParams costs
                  algoCosts = zip algoNames costsForScenario
                  methodsWithCosts = zip benchmarkedAlgorithms algoCosts
                  constrainedForMethod = map (\(methodInOut, cost) -> (methodInOut, if methodPredicate $ methodInOut !! 0 then cost else (fst cost, maxCost))) methodsWithCosts
                  constrainedForInput  = map (\(methodInOut, cost) -> (methodInOut, if inPredicate $ methodInOut !! 1 then cost else (fst cost, maxCost))) constrainedForMethod
                  constrainedForOutput = map (\(methodInOut, cost) -> (methodInOut, if outPredicate $ methodInOut !! 2 then cost else (fst cost, maxCost))) constrainedForInput
                  constrainedCosts = map (snd . snd) constrainedForOutput
              in Right $ PBQPNode (length constrainedCosts) constrainedCosts
          else -- we have a scenarioIndex that is in range, but it might not be a convolution
            if (isConvolution scenarioParams) then
              if (isNothing $ Map.lookup scenarioParams costs) then
                error $ "Costs missing for convolution node " ++ show scenarioParams ++ "\n" ++ "Cost table keys:\n" ++ (intercalate "\n" $ fmap show $ Map.keys costs)
              else -- we found a cost table entry for this node
                let costsForScenario = fromJust $ Map.lookup scenarioParams costs
                    algoCosts = zip algoNames costsForScenario
                    methodsWithCosts = zip benchmarkedAlgorithms algoCosts
                    constrainedForMethod = map (\(methodInOut, cost) -> (methodInOut, if methodPredicate $ methodInOut !! 0 then cost else (fst cost, maxCost))) methodsWithCosts
                    constrainedForInput  = map (\(methodInOut, cost) -> (methodInOut, if inPredicate $ methodInOut !! 1 then cost else (fst cost, maxCost))) constrainedForMethod
                    constrainedForOutput = map (\(methodInOut, cost) -> (methodInOut, if outPredicate $ methodInOut !! 2 then cost else (fst cost, maxCost))) constrainedForInput
                    constrainedCosts = map (snd . snd) constrainedForOutput
                in Right $ PBQPNode (length constrainedCosts) constrainedCosts
            else -- this is not a convolution, so emit the default cost for non-convolutional nodes
              Left $ PBQPNode (length benchmarkedAlgorithms) (replicate (length benchmarkedAlgorithms) 0)

      buildPBQPEdge :: [Scenario] -> [((String, [Int]), Double)]-> [[String]] -> (Int, Int) -> PBQPEdge
      buildPBQPEdge scenarios edgeCosts benchmarkedAlgorithms (from, to) =
        if length scenarios <= to then
          error $ "Cost tables contain no data for scenario " ++ (show to)
        else if length scenarios <= from then
          error $ "Cost tables contain no data for scenario " ++ (show from)
        else
          let allMethods = map head benchmarkedAlgorithms
              allCombinations = [(a, b) | a <- allMethods, b <- allMethods]
              realCosts = map (getCost edgeCosts benchmarkedAlgorithms $ scenarios !! to) allCombinations
          in PBQPEdge from to (length allMethods) (length allMethods) $ map (maybe 0 id) realCosts
        where
          getCost costs constraints scenario (srcConvAlgo, dstConvAlgo) = do
            [_,_,inFmt] <- find ((==srcConvAlgo) . head) constraints
            [_,outFmt,_] <- find ((==dstConvAlgo) . head) constraints
            let transform = inFmt ++ "-to-" ++ outFmt
            let timings = filter ((==transform) . fst . fst) costs
            when (null timings) (error $ "Cost tables contain no data for transform " ++ transform)
            let [c, h, w] = [channels scenario, height scenario, width scenario]
            exactMatch <- find ((==[c, h, w]) . snd . fst) timings
            return $ snd exactMatch

      -- an edge cost is identified by a transformation name, and a scenario
      parseEdges :: Double -> [[String]] -> [((String, [Int]), Double)]
      parseEdges cscale csvText =
        let paramLines = take 3 csvText
            paramsByScenario = transpose $ map (drop 1) paramLines
            costLines  = drop 3 csvText
        in concat $ map (buildEdgeCost cscale paramsByScenario) costLines

      buildEdgeCost cscale params (cname:cvalues) =
        let cnames = replicate (length cvalues) cname
            costKeys = zip cnames $ map (map read) params
        in zip costKeys $ map ((*cscale) . read) cvalues

      warnSynthesizedCosts :: Scenario -> Either PBQPNode PBQPNode -> IO ()
      warnSynthesizedCosts _ (Right _) = return ()
      warnSynthesizedCosts sparams (Left n) = do
        if isConvolution sparams then
          hPutStrLn stderr $ "warning: some costs synthesized by heuristic for node " ++ show sparams
        else return ()

instantiateMode impl topoPath algorithmsCSVPath methodCSVPath solutionPath layerIDsPath = do
  topoFile <- liftM (filter (not . null) . lines) $ readFile topoPath
  let [nodes, parameters, edges] :: [Int] = take 3 $ map read $ words $ topoFile !! 0
  let topoNodes :: [Scenario] = map read $ take nodes $ drop 1 topoFile
  let topoEdges :: [(Int, Int)] = map (\[a, b] -> (read a, read b)) $ map words $ take edges $ drop (1+nodes) topoFile

  algorithmsCSVFile <- readFile algorithmsCSVPath
  let algorithmsCSVText :: [[String]] = parseCSV algorithmsCSVFile
  let algorithmsCSVData :: [[String]] = algorithmsCSVText

  methodCSVFile <- readFile methodCSVPath
  let methodCSVText :: [(String, String)] = parseMethodCSV methodCSVFile
  let methodCSVData :: Map.Map String String = Map.fromList methodCSVText

  solutionFile <- liftM lines $ readFile solutionPath
  let solutionText :: [String] = take nodes $ filter (not . null) solutionFile
  let solutionData :: [Int] = map read solutionText

  let algoIndices = map (\x -> (x, if x < length algorithmsCSVData then Just (algorithmsCSVData !! x) else Nothing)) solutionData

  if any (isNothing . snd) algoIndices then do
    let unknownAlgoNodes = filter (isNothing . snd . snd) $ zip [0..] algoIndices
    let errMesg = intercalate "\n" $ map (\(node, (algo, _)) -> "Trying to instantiate a solution using an unknown algorithm (" ++ show algo ++ ") for node " ++ show node) unknownAlgoNodes
    error errMesg
  else do
    let methodNames = map ((flip Map.lookup methodCSVData) . head . (algorithmsCSVData !!)) solutionData
    let inputLayouts = map (head . tail . (algorithmsCSVData !!)) solutionData
    let outputLayouts = map (head . tail . tail . (algorithmsCSVData !!)) solutionData
    let kernelLayouts = map (head . tail . tail . tail . (algorithmsCSVData !!)) solutionData

    layerIDsFile :: String <- readFile layerIDsPath
    let layerIDsText :: [String] = map (takeWhile (not . isSpace) . dropWhile isSpace) $ filter (not . null) $ lines layerIDsFile
    let layerIDs :: Map.Map Int String = Map.fromList $ zip [0..] layerIDsText

    if impl /= TRINNITY then do
      if impl == MKLDNN then do
        let methodNames = cycle [Just "triNNity::generic::CONV_MKLDNN_DIRECT"]
        let inputLayouts = cycle ["chw"]
        let outputLayouts = cycle ["chw"]
        let kernelLayouts = cycle ["oihw"]
        mapM_ (putStrLn . emitCode layerIDs) $ zip [0..] $ take nodes $ zip5 topoNodes kernelLayouts methodNames inputLayouts outputLayouts
      else if impl == ARMCL then do
        let methodNames = cycle [Just "triNNity::generic::CONV_ARMCL_DIRECT"]
        let inputLayouts = cycle ["chw"]
        let outputLayouts = cycle ["chw"]
        let kernelLayouts = cycle ["oihw"]
        mapM_ (putStrLn . emitCode layerIDs) $ zip [0..] $ take nodes $ zip5 topoNodes kernelLayouts methodNames inputLayouts outputLayouts
      else do
        error "Unrecognised external library implementation requested"
    else do
      if any (isNothing . snd) $ zip solutionData methodNames then
        error $ "Couldn't lookup method for selection with index " ++ (show $ fst $ head $ filter (isNothing . snd) $ zip solutionData methodNames)
      else
        mapM_ (putStrLn . emitCode layerIDs) $ zip [0..] $ zip5 topoNodes kernelLayouts methodNames inputLayouts outputLayouts
  where
    emitCode _ (ix, (_, _, Nothing, input, output_)) = error $ "emitCode: passed Nothing as method for index " ++ show ix
    emitCode layerNames (ix, (_, kernelLayout, method, input, output)) =
      "#define LAYER_" ++ ((map toUpper . fromJust . flip Map.lookup layerNames) ix) ++ "_IN_FMT triNNity::layout::" ++ map toUpper input ++ "\n" ++
      "#define LAYER_" ++ ((map toUpper . fromJust . flip Map.lookup layerNames) ix) ++ "_KERN_FMT triNNity::layout::" ++ map toUpper kernelLayout ++ "\n" ++
      "#define LAYER_" ++ ((map toUpper . fromJust . flip Map.lookup layerNames) ix) ++ "_METHOD " ++ fromJust method ++ "\n" ++
      "#define LAYER_" ++ ((map toUpper . fromJust . flip Map.lookup layerNames) ix) ++ "_OUT_FMT triNNity::layout::" ++ map toUpper output ++ "\n"
